var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var Mitarbeiter = require('./models/Mitarbeiter');
var mongoose = require('mongoose');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var abteilungRouter = require('./routes/abteilung');
var schichtRouter = require('./routes/schicht');
var authRouter = require('./routes/auth');
var dpRouter = require('./routes/dienstplan');

var app = express();

app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// auth middleware
app.use( async (req, res, next) => {
  authToken = req.cookies['dpAuthToken'];
  if (authToken) {
    try {
      user = await Mitarbeiter.getMitarbeiter({authToken: authToken});
      req.userRole = user.rolle;
    } catch(err) {
      console.log(err);
      res.clearCookie('dpAuthToken');
    };
  } else {
    req.userRole = ''
  };
  next();
});

// pretty HTML
app.locals.pretty = true;

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/abteilungen', abteilungRouter);
app.use('/auth', authRouter);
app.use('/schichten', schichtRouter);
app.use('/dienstplan', dpRouter);

// DB connection
mongoose.connect(
  'mongodb://db_dienstplaner:27017/dienstplan',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  }, async () => {
    console.log('connected to DB');
    if ((await Mitarbeiter.getMitarbeiter()).length < 1) {
      await Mitarbeiter.setMitarbeiter({
        username: 'admin',
        password: 'admin',
        name: 'admin',
        vorname: 'admin',
        mail: 'admin@example.com',
        rolle: 'Administrator'
      })
    };
  }
);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', {title: 'Error'});
});

module.exports = app;
