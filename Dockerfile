FROM node:14
# Create workdir in image
WORKDIR /opt/dienstplaner
# Copy source code into docker image
COPY . /opt/dienstplaner
# install all dependencies
RUN npm install
