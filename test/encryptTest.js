var assert = require('chai').assert;
var generateHashedPassword = require('../models/Mitarbeiter').generateHashedPassword;


describe('generateHashedPassword', function() {
    it("generateHashedPassword('test') soll das verschlüsselte Passwort ausgeben", function() {
        var decrypted = 'test'
        var encrypted = 'n4bQgYhMfWWaL+qgxVrQFaO/TxsrC4Is0V1sFbDwCgg='
        assert.equal(generateHashedPassword(decrypted), encrypted);
    })
})