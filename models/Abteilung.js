var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AbteilungSchema = Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
});


AbteilungSchema.statics.getAbteilung = async function(data) {
    if (data) {
        return await this.findOne({name: data.name}).exec();
    } else {
        return await this.find().exec();
    };
};


AbteilungSchema.statics.updateAbteilung = async function(curName, newName) {
    await this.findOneAndUpdate({name: curName}, {name: newName});
};


AbteilungSchema.statics.deleteAbteilung = async function(name) {
    await this.findOneAndRemove({name: name});
};

module.exports = mongoose.model('Abteilung', AbteilungSchema);