var mongoose = require('mongoose');
var crypto = require('crypto');
var Abteilung = require('../models/Abteilung');


var Schema = mongoose.Schema;

const MitarbeiterSchema = Schema({
    username: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    vorname: {
        type: String,
        required: true,
    },
    mail: {
        type: String,
        required: true,
    },
    rolle: {
        type: String,
        required: true,
        enum: ['Benutzer', 'Bearbeiter', 'Administrator'],
        default: 'Benutzer'
    },
    abteilung: {
        type: Schema.Types.ObjectId,
        ref: 'Abteilung'
    },
    authToken: {
        type:String
    }
});


MitarbeiterSchema.statics.getMitarbeiter = async function(data) {
    if (data) {
        var query = {};
        if (data.id) {query._id = data.id};
        if (data.username){query.username = data.username};
        if (data.password) {query.password = data.password};
        if (data.name) {query.name = data.name};
        if (data.vorname) {query.vorname = data.vorname};
        if (data.mail) {query.mail = data.mail};
        if (data.abteilung) {query.abteilung = data.abteilung};
        if (data.rolle) {query.rolle = data.rolle};
        if (data.authToken) {query.authToken = data.authToken};
        return await this.findOne(query).populate('abteilung').exec();
    } else {
        return await this.find().populate('abteilung').exec();
    };
};


MitarbeiterSchema.statics.getRoles = async function() {
    return await this.schema.path('rolle').enumValues;
};


MitarbeiterSchema.statics.setMitarbeiter = async function(data) {
    var user = new this({
        username: data.username,
        password: this.generateHashedPassword(data.password),
        name: data.name,
        vorname: data.vorname,
        mail: data.mail,
        abteilung: data.abteilung,
        rolle: data.rolle,
    });
    try {
        await user.save();
        return true;
    } catch (err) {
        return false;
    }
};


MitarbeiterSchema.statics.updateMitarbeiter = async function(data) {
    var query = {};
    if (data.newUname) {query.username = data.newUname};
    if (data.password) {query.password = this.generateHashedPassword(data.password)};
    if (data.name) {query.name = data.name};
    if (data.vorname) {query.vorname = data.vorname};
    if (data.mail) {query.mail = data.mail};
    if (data.abteilung) {query.abteilung = data.abteilung};
    if (data.rolle) {query.rolle = data.rolle};
    if (data.authToken) {query.authToken = data.authToken};

    await this.findOneAndUpdate({username: data.curUname}, query);
};


MitarbeiterSchema.statics.deleteMitarbeiter = async function(data) {
    await this.findOneAndRemove({username: data.curUname});
};


MitarbeiterSchema.statics.generateHashedPassword = function(password) {
    var sha256 = crypto.createHash('sha256');
    var hash = sha256.update(password).digest('base64');
    return hash;
};


MitarbeiterSchema.statics.generateAuthToken = function() {
    var token = crypto.randomBytes(30).toString('hex');
    return token;
}

module.exports = mongoose.model('Mitarbeiter', MitarbeiterSchema);
