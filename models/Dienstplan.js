var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var DienstplanSchema = Schema({
    date: {
        type: Date,
        required: true
    },
    mitarbeiter: {
        type: Schema.Types.ObjectId,
        ref: 'Mitarbeiter',
        required: true
    },
    schicht: {
        type: Schema.Types.ObjectId,
        ref: 'Schicht',
        required: true
    }
});


DienstplanSchema.statics.getDienstplan = async function(data) {
    if (data) {
        var query = {};
        if (data.id) {query._id = data.id};
        if (data.date) {query.date = data.date};
        if (data.mitarbeiter){query.mitarbeiter = data.mitarbeiter};
        if (data.schicht) {query.password = data.schicht};
        return await this.findOne(query).populate({path: 'mitarbeiter', populate: {path: 'abteilung'}}).populate('schicht').exec();
    } else {
        return await this.find().populate('mitarbeiter').populate('abteilung').populate('schicht').exec();
    };
};


DienstplanSchema.statics.setDienstplan = async function(data) {
    var dp = new this({
        date: data.date,
        mitarbeiter: data.mitarbeiter,
        schicht: data.schicht
    });
    try {
        await dp.save();
        return true;
    } catch (err) {
        return false;
    }
};


DienstplanSchema.statics.updateDienstplan = async function(data) {
    var query = {
        date: data.date,
        mitarbeiter: data.mitarbeiter,
        schicht: data.schicht
    };

    await this.findOneAndUpdate({_id: data.id}, query);
};


DienstplanSchema.statics.deleteDienstplan = async function(data) {
    await this.findOneAndRemove({_id: data.id});
};

module.exports = mongoose.model('Dienstplan', DienstplanSchema);