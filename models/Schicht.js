var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var SchichtSchema = Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    startzeit: {
        type: Date,
        required: true,
    },
    endzeit: {
        type: Date,
        required: true,
    }
});


SchichtSchema.statics.getSchicht = async function(data) {
    if (data) {
        var query = {};
        if (data.id) {query._id = data.id};
        if (data.name) {query.name = data.name};
        if (data.start) {query.startzeit = data.start};
        if (data.end) {query.endzeit = data.start};
        return await this.findOne(query);
    } else {
        return await this.find().exec();
    }
};


SchichtSchema.statics.updateSchicht = async function(data) {
    await this.findOneAndUpdate({name:data.curName}, {name:data.newName,startzeit:data.startzeit,endzeit:data.endzeit});
};


SchichtSchema.statics.deleteSchicht = async function(data) {
    await this.findOneAndRemove({name: data.name});
};


module.exports = mongoose.model('Schicht', SchichtSchema);