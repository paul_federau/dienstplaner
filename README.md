# Dienstplaner

Dienstplaner als Webanwendung

# Installation

1. ### Docker & Docker-Compose installieren

2. ### docker-compose.yml downloaden:
    [Download](https://gitlab.com/paul_federau/dienstplaner/-/blob/46582f1435ac4fd782c04ca368e677ae030e682e/docker-compose.yml)

3. ### Docker-Compose starten
    ```
    sudo docker-compose up -d
    ```
4. ### Standardlogin für Dienstplaner
    ```
    Benutzername: admin
    Passwort: admin
    ```
