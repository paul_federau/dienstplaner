var express = require('express');
var Abteilung = require('../models/Abteilung');


var router = express.Router();


// Alle Abteilungen anzeigen
router.get('/', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var abt = await Abteilung.getAbteilung();

        res.render('abteilung/showAbteilung', {
            data: {
                title: 'Abteilungsmanagement',
                abteilungen: abt
            },
        });
    } else {
        res.redirect('/');
    };
});


// Formular für Erstellen von Abteilungen anzeigen
router.get('/create', (req, res) => {
    if (req.userRole) {
        res.render('abteilung/createAbteilung', {
            data: {
                title: 'Abteilung erstellen',
            }
        });
    } else {
        res.redirect('/');
    };
});


// Post Request für Erstellen von Abteilungen
router.post('/create', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var abtName = req.body.name;

        try {
            var newAbt = new Abteilung({
                name: abtName
            });
            await newAbt.save();
            res.redirect('/abteilungen');
        } catch (err) {
            res.render('abteilung/createAbteilung', {
                data: {
                    title: 'Abteilung erstellen',
                    error: 'Abteilung bereits vorhanden'
                }
            });
        }
    }
});


// Anzeige für Bearbeiten von Abteilung
router.get('/edit', (req, res) => {
    if (req.userRole === 'Administrator') {
        var curName = req.query.name;

        res.render('abteilung/editAbteilung', {
            data: {
                title: 'Abteilung bearbeiten',
                curName: curName
            }
        });
    } else {
        res.redirect('/');
    };
});


// Post Request für Bearbeiten von Abteilung
router.post('/edit', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var curName = req.query.name;
        var newName = req.body.name;

        try {
            await Abteilung.updateAbteilung(curName, newName);
            res.redirect('/abteilungen');
        } catch (err) {
            res.render('abteilung/editAbteilung', {
                data: {
                    title: 'Abteilung bearbeiten',
                    error: 'Änderungen konnten nicht gespeichert werden'
                }
            });
        };
    } else {
        res.redirect('/');
    };
});


// Abteilung löschen
router.get('/delete', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var name = req.query.name;
        await Abteilung.deleteAbteilung(name);
        res.redirect('/abteilungen');
    } else {
        res.redirect('/');
    };
});

module.exports = router;
