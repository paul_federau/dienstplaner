var express = require('express');
var Dienstplan = require('../models/Dienstplan');
var Mitarbeiter = require('../models/Mitarbeiter');
var Schicht = require('../models/Schicht');


var router = express.Router();


// Alle Dienstplan Einträge anzeigen
router.get('/', async (req, res) => {
    if (req.userRole === 'Administrator' || req.userRole === 'Bearbeiter') {
        var dates = await Dienstplan.getDienstplan();

        res.render('dienstplan/showDp', {
            data: {
                title: 'Dienstplanmanagement',
                dates: dates
            }
        });
    } else {
        res.redirect('/');
    }
});


// Anzeige für Erstellen von Dienstplaneintrag
router.get('/create', async (req, res) => {
    if (req.userRole === 'Administrator' || req.userRole === 'Bearbeiter') {
        var ma = await Mitarbeiter.getMitarbeiter();
        var schichten = await Schicht.getSchicht();

        res.render('dienstplan/createDp', {
            data: {
                title: 'DP Eintrag erstellen',
                ma: ma,
                schichten: schichten
            }
        });
    } else {
        res.redirect('/');
    }
});


// Post Request für Erstellen von Dienstplaneintrag
router.post('/create', async (req, res) => {
    if (req.userRole === 'Administrator' || req.userRole === 'Bearbeiter') {
        var entry = {};
        entry.date = req.body.date;
        entry.mitarbeiter = await Mitarbeiter.getMitarbeiter({username: req.body.ma});
        entry.schicht = await Schicht.getSchicht({name: req.body.schicht});

        if(! await Dienstplan.getDienstplan({date: entry.date, mitarbeiter: entry.mitarbeiter})) {
            if (await Dienstplan.setDienstplan(entry)) {
                res.redirect('/dienstplan');
            } else {
                var ma = await Mitarbeiter.getMitarbeiter();
                var schichten = await Schicht.getSchicht();

                res.render('dienstplan/createDp', {
                    data: {
                        title: 'Dienstplaneintrag erstellen',
                        ma: ma,
                        schichten: schichten,
                        error: 'Dienstplaneintrag konnte nicht gespeichert werden!'
                    }
                });
            };
        } else {
            var ma = await Mitarbeiter.getMitarbeiter();
            var schichten = await Schicht.getSchicht();

            res.render('dienstplan/createDp', {
                data: {
                    title: 'DP Eintrag erstellen',
                    ma: ma,
                    schichten: schichten,
                    error: 'Benutzer hat an diesem Tag bereits eine Schicht'
                }
            });
        };
    } else {
        res.redirect('/');
    };
});


// Anzeige für Bearbeiten von Dienstplaneintrag
router.get('/edit', async (req, res) => {
    if (req.userRole === 'Administrator' || req.userRole === 'Bearbeiter') {
        var id = req.query.id;
        var entry = await Dienstplan.getDienstplan({id: id});
        var ma = await Mitarbeiter.getMitarbeiter();
        var schichten = await Schicht.getSchicht();
        var date = entry.date.toISOString().slice(0,10);

        res.render('dienstplan/editDp', {
            data: {
                title: 'DP Eintrag bearbeiten',
                entry: entry,
                date,
                ma: ma,
                schichten: schichten
            }
        });
    } else {
        res.redirect('/');
    };
});


// Post Request für Bearbeiten von Dienstplaneintrag
router.post('/edit', async (req, res) => {
    if (req.userRole === 'Administrator' || req.userRole === 'Bearbeiter') {
        var entry = {};
        entry.id = req.query.id;
        entry.date = req.body.date;
        entry.mitarbeiter = (await Mitarbeiter.getMitarbeiter({username: req.body.ma}))._id;
        entry.schicht = (await Schicht.getSchicht({name: req.body.schicht}))._id;
        var id = req.query.id;
        var date = req.body.date;
        var ma = req.body.ma;
        var schicht = req.body.schicht;

        try {
            await Dienstplan.updateDienstplan(entry);
            res.redirect('/dienstplan');
        } catch (err) {
            var ma = await Mitarbeiter.getMitarbeiter();
            var schichten = await Schicht.getSchicht();
            res.render('dienstplan/createDp', {
                data: {
                    title: 'Dienstplaneintrag erstellen',
                    ma: ma,
                    schichten: schichten,
                    error: 'Dienstplaneintrag konnte nicht gespeichert werden!'
                }
            });
        };
    } else {
        res.redirect('/');
    }
});


// Dienstplan löschen
router.get('/delete', async (req, res) => {
    if (req.userRole === 'Administrator' || req.userRole === 'Bearbeiter') {
        var id = req.query.id;
        await Dienstplan.deleteDienstplan({id: id});
        res.redirect('/dienstplan');
    } else {
        res.redirect('/');
    };
});

module.exports = router;
