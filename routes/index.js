var express = require('express');
var Dienstplan = require('../models/Dienstplan');
var Mitarbeiter = require('../models/Mitarbeiter');


var router = express.Router();


// Startseite anzeigen
router.get('/', async (req, res) => {
	if (req.userRole === 'Administrator' || req.userRole === 'Bearbeiter' || req.userRole === 'Benutzer') {
		var ma = await Mitarbeiter.getMitarbeiter();

		var curDate = new Date;
		var firstDay = curDate.getDate() - curDate.getDay() + 1;
		var dates = [];

		for (var i = 0; i < 14; i++) {
			var userSchicht = {};
			var date = new Date();

			date.setDate(firstDay + i);
			date.setHours(0,0,0,0);

			for (var j = 0; j < ma.length; j++) {
				var dp = await Dienstplan.getDienstplan({date: date, mitarbeiter: ma[j]});

				if (dp) {
					userSchicht[ma[j].username] = {
						mitarbeiter: dp.mitarbeiter,
						schicht: dp.schicht,
						hasEntry: true
					}
				} else {
					userSchicht[ma[j].username] = {
						hasEntry: false
					}
				};
			};
			dates.push({date:date, userSchicht:userSchicht});
		};

  		res.render('index', { 
    		data: {
				title: 'Dienstplaner',
				mitarbeiter: ma,
				dates: dates
    		}
	  	});
	} else {
		res.render('index', {
			data: {
				title: 'Dienstplaner',
				noAuth: true
			}
		});
	};
});


module.exports = router;
