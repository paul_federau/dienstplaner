var express = require('express');
var Schicht = require('../models/Schicht');


var router = express.Router();


// Alle Schichten anzeigen
router.get('/', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var schichten = await Schicht.getSchicht();

        res.render('schicht/showSchicht', {
            data: {
                title: 'Schichtmanagement',
                schichten: schichten
            }
        })
    } else {
        res.redirect('/');
    };
});


// Anzeige für Erstellen von Schichten
router.get('/create', async (req, res) => {
    if (req.userRole === 'Administrator') {
        res.render('schicht/createSchicht', {
            data: {
                title: 'Schicht erstellen'
            }
        });
    } else {
        res.redirect('/');
    };
});


// Post Request für Erstellen von Schichten
router.post('/create', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var name = req.body.name;
        var start = req.body.startzeit;
        var end = req.body.endzeit;

        try {
            var newSchicht = new Schicht({
                name: name,
                startzeit: new Date('1-1-1 ' + start),
                endzeit: new Date('1-1-1 ' + end)
            });
            await newSchicht.save();
            res.redirect('/schichten');
        } catch (err) {
            res.render('schicht/createSchicht', {
                data: {
                    title: 'Schicht erstellen',
                    error: 'Schicht bereits vorhanden'
                }
            });
        };
    };
});


// Anzeige für Schicht bearbeiten
router.get('/edit', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var curName = req.query.name;

        res.render('schicht/editSchicht', {
            data: {
                title: 'Schicht bearbeiten',
                curName: curName
            }
        });
    } else {
        res.redirect('/');
    };
});


// Post Request für Schicht bearbeiten
router.post('/edit', async (req, res) => {
    if (req.userRole === 'Administrator') {
        try {
            var curName = req.query.name;
            var newName = req.body.name;
            var start = new Date('1-1-1 ' + req.body.startzeit);
            var end = new Date('1-1-1 ' + req.body.endzeit);

            var data = {
                curName: curName,
                newName: newName,
                startzeit: start,
                endzeit: end
            }

            await Schicht.updateSchicht(data);
            res.redirect('/schichten');
        } catch (err) {
            res.render('schicht/editSchicht', {
                data: {
                    title: 'Schicht erstellen',
                    curName: curName,
                    error: 'Fehler beim bearbeiten der Schicht'
                }
            });
        };
    } else {
        res.redirect('/');
    };
});


// Schicht löschen
router.get('/delete', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var name = req.query.name;

        await Schicht.deleteSchicht({name: name});
        res.redirect('/schichten');
    } else {
        res.redirect('/');
    };
});

module.exports = router;
