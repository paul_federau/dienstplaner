var express = require('express');
var crypto = require('crypto');
var Mitarbeiter = require('../models/Mitarbeiter');


var router = express.Router();


// Anzeige für Benutzer einloggen
router.get('/login', async (req, res) => {
	if (req.userRole === '') {
		res.render('auth/login', {
			data: {
				title: 'Login',
				noAuth : true
			}
		});
	}
});


// Post Request für Benutzer einloggen
router.post('/login', async (req, res) => {
	if (req.userRole === '') {
		var username = req.body.username;
		var hashedPassword = Mitarbeiter.generateHashedPassword(req.body.password);
		var user = await Mitarbeiter.findOne({username: username, password: hashedPassword});
		if (user) {
			authToken = await Mitarbeiter.generateAuthToken();
			await Mitarbeiter.updateMitarbeiter({curUname: user.username, authToken: authToken});
			res.cookie('dpAuthToken', authToken);
			res.redirect('/');
		} else {
			res.render('login', {
				data: {
					title: 'Login',
					error: 'Benutzername oder Passwort falsch',
					noAuth: true
				}
			});
		};
	}
});


// Benutzer ausloggen
router.get('/logout', async (req, res) => {
	res.clearCookie('dpAuthToken');
	res.redirect('/');
});

module.exports = router;
