var express = require('express');
var Mitarbeiter = require('../models/Mitarbeiter');
var Abteilung = require('../models/Abteilung');


var router = express.Router();

// Alle Benutzer anzeigen
router.get('/', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var users = await Mitarbeiter.getMitarbeiter();

        res.render('mitarbeiter/showUser', {
            data: {
                title: 'Benutzerverwaltung',
                users: users
            }
        });
    } else {
        res.redirect('/');
    };
});


// Anzeige für Erstellen von Benutzer
router.get('/create', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var abt = await Abteilung.getAbteilung();
        var roles = await Mitarbeiter.getRoles();

        res.render('mitarbeiter/createUser', {
            data: {
                title: 'Benutzer erstellen',
                abteilungen: abt,
                rollen: roles
            }
        });
    } else {
        res.redirect('/');
    }
});


// Post Request für Erstellen von Benutzer
router.post('/create', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var user = {};
        user.username = req.body.username;
        user.password = req.body.password;
        user.name = req.body.nachname;
        user.vorname = req.body.vorname;
        user.mail = req.body.mail;
        user.rolle = req.body.rolle;
        user.abteilung = (await Abteilung.getAbteilung({name: req.body.abteilung}))._id;

        if (! await Mitarbeiter.getMitarbeiter({username: user.username})) {
            if (await Mitarbeiter.setMitarbeiter(user)) {
                res.redirect('/users');
            };
        } else {
            var abt = await Abteilung.getAbteilung();
            var roles = await Mitarbeiter.getRoles();

            res.render('mitarbeiter/createUser', {
                data: {
                    title: 'Benutzer erstellen',
                    abteilungen: abt,
                    rollen: roles,
                    error: 'Benutzername bereits vorhanden!'
                }
            });
        };
    } else {
        res.redirect('/');
    };
});


// Anzeige für Benutzer bearbeiten
router.get('/edit', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var abt = await Abteilung.getAbteilung();
        var roles = await Mitarbeiter.getRoles();
        var curUname = req.query.username;

        res.render('mitarbeiter/editUser', {
            data: {
                title: 'Benutzer bearbeiten',
                curUsername: curUname,
                abteilungen: abt,
                rollen: roles
            }
        });
    } else {
        res.redirect('/');
    };
});


// Post Request für Bearbeiten von Benutzer
router.post('/edit', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var user = {}
        user.curUname = req.query.username;
        user.newUname = req.body.username;
        user.password = req.body.password;
        user.name = req.body.nachname;
        user.vorname = req.body.vorname;
        user.mail = req.body.mail;
        user.rolle = req.body.rolle;
        user.abteilung = (await Abteilung.getAbteilung({name: req.body.abteilung}))._id;

        try {
            await Mitarbeiter.updateMitarbeiter(user);
            res.redirect('/users');
        } catch (err) {
            var abt = await Abteilung.getAbteilung();
            var roles = await Mitarbeiter.getRoles();
        
            res.render('mitarbeiter/editUser', {
                data: {
                    title: 'Benutzer bearbeiten',
                    abteilungen: abt,
                    curUsername: user.curUname,
                    rollen: roles,
                    error: 'Benutzer konnte nicht gespeichert werden!'
                }
            });
        };
    } else {
        res.redirect('/');
    }
});


// Benutzer löschen
router.get('/delete', async (req, res) => {
    if (req.userRole === 'Administrator') {
        var curUname = req.query.username;
  
        await Mitarbeiter.deleteMitarbeiter({curUname: curUname});
        res.redirect('/users');
    } else {
        res.redirect('/');
    };
  });

module.exports = router;
