var elements = document.getElementsByClassName('date');

for (var i = 0; i < elements.length; i++) {
    var dt = new Date(elements[i].innerHTML);
    elements[i].innerHTML = dt.toLocaleDateString();
};